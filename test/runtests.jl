using MS_Import
using Test
# include("/Users/saersamanipour/Desktop/dev/pkgs/ms_import.jl/src/MS_Import.jl")

@testset "import_files_MS1 (mzXML)" begin
    # Write your own tests here.''
    pathin=""
    # pathin = "/Users/saersamanipour/Desktop/dev/pkgs/ms_import.jl/test"
    filenames=["test_file.mzXML"]
    mz_thresh=[100,150]
    Int_thresh = 100
    # chrom = MS_Import.import_files(pathin,filenames,mz_thresh,Int_thresh)

    mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,
    Rt,centroid = import_files_MS1(pathin,filenames,mz_thresh,Int_thresh)
    println(typeof(mz_vals))
    #println(mz_vals)

    @test size(mz_vals,1) == 43

end


@testset "import_files_MS1 (CDF)" begin
    # Write your own tests here.''
    pathin=""
    filenames=["test_file.CDF"]
    mz_thresh=[100,150]
    mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,Rt,centroid=import_files_MS1(pathin,filenames,mz_thresh)

    @test size(mz_vals,1) > 0

end


@testset "import_files" begin
    # Write your own tests here.''
    pathin=""
    filenames=["test_file.mzXML"]
    mz_thresh=[100,150]
    chrom=import_files(pathin,filenames,mz_thresh)
    #println(chrom["MS1"]["Mz_values"])

    @test chrom["MS1"]["Rt"][2] >= 6.1

    mz_thresh = [140,0]
    chrom = import_files(pathin,filenames,mz_thresh)
    @test chrom["MS1"]["Mz_values"][1,1] == 149.81692504882812

    # pathin=""
    # filenames=["test_zlib_file.mzXML"]
    # mz_thresh=[100,150]
    # chrom1=import_files(pathin,filenames,mz_thresh)

    # println(chrom1["MS1"]["Rt"])


end
