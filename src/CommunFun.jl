using Base 



########################################################################
# Array to matrix
#

function array2mat_(a)

    dummy_var=zeros(Float32,size(a,1),maximum(size.(a[1:end],1)))

    for i=1:size(a,1)
        dummy_var[i,1:length(a[i])] = a[i]
    end
    GC.gc()
    return dummy_var

end



#############################################################################
# Remove empty scans

function emp_scan_rm!(chrom)

    tv1=sum(chrom["MS1"]["Mz_intensity"], dims=2);
    chrom["MS1"]["Polarity"]=chrom["MS1"]["Polarity"][tv1[:] .> 0];
    chrom["MS1"]["TIC"]=chrom["MS1"]["TIC"][tv1[:] .> 0];
    chrom["MS1"]["Rt"]=chrom["MS1"]["Rt"][tv1[:] .> 0];
    chrom["MS1"]["BasePeak"]=chrom["MS1"]["BasePeak"][tv1[:] .> 0];
    chrom["MS1"]["PrecursorIon"]=chrom["MS1"]["PrecursorIon"][tv1[:] .> 0];
    chrom["MS1"]["Centroid"]=chrom["MS1"]["Centroid"][tv1[:] .> 0];
    chrom["MS1"]["PrecursorIonWin"]=chrom["MS1"]["PrecursorIonWin"][tv1[:] .> 0];
    chrom["MS1"]["ScanType"]=chrom["MS1"]["ScanType"][tv1[:] .> 0];
    chrom["MS1"]["Mz_values"]=chrom["MS1"]["Mz_values"][tv1[:] .> 0,:];
    chrom["MS1"]["Mz_intensity"]=chrom["MS1"]["Mz_intensity"][tv1[:] .> 0,:];

    return chrom


end # function



#############################################################################
# Removes the MS1 scans without MS2

function ms2_align!(chrom1)


    t_ms2=chrom1["MS2"]["Rt"][1]
    t_end_ms2=chrom1["MS2"]["Rt"][end]

    ind1=argmin(abs.(chrom1["MS1"]["Rt"] .- t_ms2))
    ind2=argmin(abs.(chrom1["MS1"]["Rt"] .- t_end_ms2))

    t_ms1=chrom1["MS1"]["Rt"][ind1]
    t_end_ms1=chrom1["MS1"]["Rt"][ind2]

    ind1_1=argmin(abs.(chrom1["MS2"]["Rt"] .- t_ms1))
    ind2_1=argmin(abs.(chrom1["MS2"]["Rt"] .- t_end_ms1))




    chrom1["MS1"]["Polarity"]=chrom1["MS1"]["Polarity"][ind1:ind2];
    chrom1["MS1"]["TIC"]=chrom1["MS1"]["TIC"][ind1:ind2];
    chrom1["MS1"]["Rt"]=chrom1["MS1"]["Rt"][ind1:ind2];
    chrom1["MS1"]["BasePeak"]=chrom1["MS1"]["BasePeak"][ind1:ind2];
    chrom1["MS1"]["PrecursorIon"]=chrom1["MS1"]["PrecursorIon"][ind1:ind2];
    chrom1["MS1"]["Centroid"]=chrom1["MS1"]["Centroid"][ind1:ind2];
    chrom1["MS1"]["PrecursorIonWin"]=chrom1["MS1"]["PrecursorIonWin"][ind1:ind2];
    chrom1["MS1"]["ScanType"]=chrom1["MS1"]["ScanType"][ind1:ind2];
    chrom1["MS1"]["Mz_values"]=chrom1["MS1"]["Mz_values"][ind1:ind2,:];
    chrom1["MS1"]["Mz_intensity"]=chrom1["MS1"]["Mz_intensity"][ind1:ind2,:];


    chrom1["MS2"]["Polarity"]=chrom1["MS2"]["Polarity"][ind1_1:ind2_1];
    chrom1["MS2"]["TIC"]=chrom1["MS2"]["TIC"][ind1_1:ind2_1];
    chrom1["MS2"]["Rt"]=chrom1["MS2"]["Rt"][ind1_1:ind2_1];
    chrom1["MS2"]["BasePeak"]=chrom1["MS2"]["BasePeak"][ind1_1:ind2_1];
    chrom1["MS2"]["PrecursorIon"]=chrom1["MS2"]["PrecursorIon"][ind1_1:ind2_1];
    chrom1["MS2"]["Centroid"]=chrom1["MS2"]["Centroid"][ind1_1:ind2_1];
    chrom1["MS2"]["PrecursorIonWin"]=chrom1["MS2"]["PrecursorIonWin"][ind1_1:ind2_1];
    chrom1["MS2"]["ScanType"]=chrom1["MS2"]["ScanType"][ind1_1:ind2_1];
    chrom1["MS2"]["Mz_values"]=chrom1["MS2"]["Mz_values"][ind1_1:ind2_1,:];
    chrom1["MS2"]["Mz_intensity"]=chrom1["MS2"]["Mz_intensity"][ind1_1:ind2_1,:];

    return chrom1

end # function