using BenchmarkTools
using LightXML
using Unitful
using Codecs

include("CommunFun.jl")

########################################################################################


function mzxml_read(path2mzxml,mz_thresh::Vector{Float32},Int_thresh::Float32)

    xdoc = parse_file(path2mzxml);
    xroot = root(xdoc);
    if LightXML.name(xroot) != "mzXML"
        error("Not an mzXML file")
    end
    # Find the msRun node
    msRun = find_element(xroot, "msRun");

    el_ins = find_element(msRun, "msInstrument")
    Ins = attribute(find_element(el_ins, "msModel"),"value")
    el_ion = attribute(find_element(el_ins,"msIonisation"),"value")
    el_man = attribute(find_element(el_ins,"msManufacturer"),"value")

    polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int,col_energy=read_scan(msRun,mz_thresh,Int_thresh)
    chrom=chrom_fold(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
        totIonCurrent,scan_type,centroided,mz,mz_int,col_energy)

    MS_Instrument=Dict("msModel" => Ins, "msIonisation" => el_ion, "msManufacturer" => el_man)

    chrom["MS_Instrument"]=MS_Instrument
    free(xdoc)
    GC.gc() 
    return chrom

end # function


###############################################################################
# Reading the scans
# Done

function read_scan(msRun,mz_thresh::Vector{Float32},Int_thresh::Float32)
    n = 0
    scans_started = false
    for line in child_elements(msRun)
        if scans_started || LightXML.name(line) == "scan"
            n+=1
            scans_started = true
        end
    end
    
    polarity=Vector{String}(undef, n)
    pre_mz=Vector{Float32}(undef, n)
    pre_mz_win=Vector{Float32}(undef, n)
    retentionTime=Vector{Float32}(undef,n)
    msLevel=Vector{String}(undef, n)
    basePeakMz=Vector{Float32}(undef, n)
    totIonCurrent=Vector{Float32}(undef, n)
    scan_type=Vector{String}(undef, n)
    centroided=Vector{String}(undef, n)
    mz=Vector{Vector{Float32}}(undef, n)
    mz_int=Vector{Vector{Float32}}(undef, n)
    col_energy=Vector{Float32}(undef, n)
    #c=1
    n = 1
    #global line_t 

    scans_started = false
    for line in child_elements(msRun)
        if scans_started || LightXML.name(line) == "scan"
            scans_started = true
            #println(line)
            #if n == 794
            #    line_t = line
                #println(line_t)
            #    break
            #end 
            polarity1,pre_mz1,pre_mz_win1,retentionTime1,msLevel1,basePeakMz1,
            totIonCurrent1,scan_type1,centroided1,mz1,I,colE=read_scan_info(line,mz_thresh,Int_thresh)

            #println(mz1)
            #break

            polarity[n]=polarity1
            pre_mz[n]=pre_mz1
            pre_mz_win[n]=pre_mz_win1
            retentionTime[n]=retentionTime1
            msLevel[n]=msLevel1
            basePeakMz[n]=basePeakMz1
            totIonCurrent[n]=totIonCurrent1
            scan_type[n]=scan_type1
            centroided[n]=centroided1
            mz[n]=mz1
            mz_int[n]= I
            col_energy[n]=colE
            #c=c+1
            #println(c)
            n += 1

        end

    end
    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int, col_energy)

end # function


###############################################################################
# Extracting info from the scans
# Done

function read_scan_info(line,mz_thresh::Vector{Float32},Int_thresh::Float32)

    precisiondict = Dict("32" => (Float32, Float32, Float32), "64" => (Float64, Float64, Float64))
    # line = find_element(msRun,"scan")
    # line = line_t
    polarity::String = attribute(line, "polarity")
    #polarity = attribute(line, "polarity")
    # println(line)
    
    mm=find_element(line,"precursorMz")
    pre_mz=Float32(0.0)
    pre_mz_win=Float32(0.0)
    if mm !== nothing
        pre_mz=parse(Float32,content(mm))
        if has_attribute(mm,"windowWideness")
            pre_mz_win = parse(Float32,attribute(mm,"windowWideness"))
        end
    end

    tstr::String = attribute(line, "retentionTime")
    #tstr = attribute(line, "retentionTime")
    retentionTime = parse(Float32, tstr[3:end-1])
    msLevel::String = attribute(line, "msLevel")
    #msLevel = attribute(line, "msLevel")
    basePeakMz = Float32(-100.00)
    if has_attribute(line, "basePeakMz")
        basePeakMz = parse(Float32, attribute(line, "basePeakMz"))
    end
    totIonCurrent = Float32(1.00)
    if has_attribute(line, "totIonCurrent")
       totIonCurrent = parse(Float32, attribute(line, "totIonCurrent"))
    end
    scan_type::String = attribute(line, "scanType")
    #scan_type = attribute(line, "scanType")
    centroided::String = attribute(line,"centroided")
    #centroided = attribute(line,"centroided")
    #peak::XMLElement = find_element(line, "peaks")
    peak = find_element(line, "peaks")

    # 1840-element Array{UInt8,1}:
    data = decode(Base64, content(peak))
    if attribute(peak, "compressionType") ==  "zlib"
        data = decode(Zlib, data)    
    end
    # data = decode(Zlib, decode(Base64, content(peak)))
    # println(bytestring(data))
    TI, T, nochildren = precisiondict[attribute(peak, "precision")]
    A = reinterpret(TI, data)

    bo = attribute(peak, "byteOrder")
    if bo != "network"
        error("Don't know what to do with byteOrder $bo")
    end
    ntoh!(A)
    I = A[2:2:end]
    mz1 = A[1:2:end]


    mz2=mz1[(Int_thresh .<= I) .& (mz_thresh[1].<= mz1 .<= mz_thresh[2])]
    I2=I[(Int_thresh .<= I) .& (mz_thresh[1].<= mz1 .<= mz_thresh[2])]
    collisionEnergy = NaN32

    if has_attribute(line, "collisionEnergy")
        collisionEnergy = parse(Float32, attribute(line, "collisionEnergy"))
    end


    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,totIonCurrent,
    scan_type,centroided,Float32.(mz2),Float32.(I2),collisionEnergy)
end # function


##############################################################################
# XML encoding

function ntoh!(A)
    for i = 1:length(A)
        A[i] = ntoh(A[i])
    end
end

function ntoh!(A::Base.ReinterpretArray{Float64, 1, UInt8, Vector{UInt8}, false})
    for i = eachindex(A)
        A[i] = ntoh(A[i])
    end
end

function ntoh!(A::Base.ReinterpretArray{Float32, 1, UInt8, Vector{UInt8}, false})
    for i = eachindex(A)
        A[i] = ntoh(A[i])
    end
end

##############################################################################
# Chrom folding
# Done

function chrom_fold(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int,col_energy)

    chrom=Dict{String, Any}()
    ms_levels::Vector{String}=sort(unique(msLevel))
    
    for i=1:length(ms_levels)
        chrom["MS$i"] = Dict(
            "Rt" => round.(retentionTime[msLevel .== ms_levels[i]]./60, digits=3),
            "Polarity" => polarity[msLevel .== ms_levels[i]],
            "PrecursorIon" => pre_mz[msLevel .== ms_levels[i]],
            "PrecursorIonWin" => pre_mz_win[msLevel .== ms_levels[i]],
            "BasePeak" => basePeakMz[msLevel .== ms_levels[i]],
            "TIC" => totIonCurrent[msLevel .== ms_levels[i]],
            "ScanType" => scan_type[msLevel .== ms_levels[i]],
            "Centroid" => centroided[msLevel .== ms_levels[i]],
            "Mz_values"=> Array{Float32,2}(undef,1,1),
            "Mz_intensity"=>Array{Float32,2}(undef,1,1)
        )
        mz_s=mz[msLevel .== ms_levels[i]]
        mz_int_s=mz_int[msLevel .== ms_levels[i]]
        if i == 2
            chrom["MS2"]["collisionEnergy"] = col_energy[msLevel .== ms_levels[i]]
        end
        chrom["MS$i"]["Mz_values"]=array2mat_(mz_s)
        chrom["MS$i"]["Mz_intensity"]=array2mat_(mz_int_s)


    end

    return chrom

end # function


########################################################################
# Testing area
#

"""

pathin = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/test"
pathin = "/Users/saersamanipour/Desktop/dev/pkgs/ms_import.jl/test"
filenames=["TestChrom.mzXML"]
# filenames=["test_file.mzXML"]
m=split(filenames[1],".")

path2mzxml = joinpath(pathin,filenames[])

# path2mzxml = "/Users/saersamanipour/Desktop/dev/pkgs/ULSA/test/TestChrom.mzXML"
# path2mzxml = "/Users/saersamanipour/Desktop/dev/ongoing/mzXML.jl/test/test32.mzXML"

mz_thresh = Float32.([100,150])
int_thresh = Float32(100)
# Int_thresh = Float32(100)

chrom = mzxml_read(path2mzxml,mz_thresh,int_thresh)
chrom["MS1"]["Mz_values"]

""" 