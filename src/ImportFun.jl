using BenchmarkTools
using ProgressMeter

include("NetCDFRead.jl")
include("MzXMLRead.jl")
include("CommunFun.jl")
include("MzMLRead.jl")

#####################################################################
## Wrapper to import the files

function import_files(pathin,filenames,mz_thresh=[0,0],Int_thresh=0)

    m=split(filenames[1],".")

    if m[end] == "cdf" || m[end] == "CDF"

        chrom=cdf_Import(pathin,filenames)


    elseif m[end] == "mzxml" || m[end] == "mzXML" || m[end] == "MZXML"

        if isa(filenames,Array)==1
            path_in=joinpath(pathin,filenames[])
        else
            path_in=joinpath(pathin,filenames)
        end

        mz_thresh = Float32.(mz_thresh)
        if mz_thresh[2] == 0.0
            mz_thresh[2] = Inf32
        end
        Int_thresh = Float32(Int_thresh)

        chrom = mzxml_read(path_in,mz_thresh,Int_thresh)
        # chrom["MS1"]["Mz_values"]
    elseif lowercase(m[end]) == "mzml"
        if isa(filenames,Array)==1
            path_in=joinpath(pathin,filenames[])
        else
            path_in=joinpath(pathin,filenames)
        end

        mz_thresh = Float32.(mz_thresh)
        if mz_thresh[2] == 0.0
            mz_thresh[2] = Inf32
        end
        Int_thresh = Float32(Int_thresh)
        @warn("The mzml reader is new and might get errors\nIMS data has not been tested yet")
        chrom = mzml_read(path_in, mz_thresh, Int_thresh)
    end

    return (chrom)

end




##########################################################################
# MS1 import

function import_files_MS1(pathin,filenames,mz_thresh=[0,0],Int_thresh=0)

    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end


    if m[2] == "cdf" || m[2] == "CDF"

        chrom=cdf_Import(pathin,filenames)
        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        t0=chrom["MS1"]["t0"][1]
        t_end=chrom["MS1"]["t_end"][1]
        Rt=chrom["MS1"]["Rt"]
        msModel="NA"
        msIonisation="NA"
        msManufacturer="NA"
        polarity="NA"
        centroid = "NA"

    elseif m[2] == "mzxml" || m[2] == "mzXML" || m[2] == "MZXML"
        if isa(filenames,Array)==1
            path_in=joinpath(pathin,filenames[])
        else
            path_in=joinpath(pathin,filenames)
        end

        mz_thresh = Float32.(mz_thresh)
        if mz_thresh[2] == 0.0
            mz_thresh[2] = Inf32
        end
        Int_thresh = Float32(Int_thresh)

        chrom = mzxml_read(path_in,mz_thresh,Int_thresh)


        mz_vals = chrom["MS1"]["Mz_values"]
        mz_int = chrom["MS1"]["Mz_intensity"]
        t0 = chrom["MS1"]["Rt"][1]
        t_end = chrom["MS1"]["Rt"][end]
        Rt = chrom["MS1"]["Rt"]
        msModel = chrom["MS_Instrument"]["msModel"]
        msIonisation = chrom["MS_Instrument"]["msIonisation"]
        msManufacturer = chrom["MS_Instrument"]["msManufacturer"]
        polarity = chrom["MS1"]["Polarity"][1]
        centroid = chrom["MS1"]["Centroid"]

    end

    return(mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,Rt,centroid)


end  
    

######################################################################
## Import function batch

function import_files_batch(pathin,mz_thresh=[0,0],Int_thresh=0)


    Dname=readdir(pathin)
    ind=zeros(length(Dname),1)
    # i=1
    for i=1:length(Dname)
        if Dname[i][end-4:end] == "mzXML" || Dname[i][end-4:end]== "mzxml" || Dname[i][end-4:end] == "MZXML"
            ind[i]=1
        elseif Dname[i][end-3:end] == "CDF" || Dname[i][end-3:end]== "cdf"
            ind[i]=1

        end

    end
    ind_s=findall(x -> x > 0,ind)
    chrom_mat=Array{Any}(undef,size(ind_s,1))



    for i=1:size(ind_s,1)


        filenames=[Dname[ind_s[i]]]
        chrom_mat[i]=import_files(pathin,filenames,mz_thresh,Int_thresh)

    end


    return (chrom_mat)

end



######################################################################
## Import function batch

function import_files_batch_MS1(pathin,mz_thresh=[0,0],Int_thresh=0)


    Dname=readdir(pathin)
    ind=zeros(length(Dname),1)

    @showprogress 1 "Importing the files..." for i=1:length(Dname)
        sleep(0.1)
        if Dname[i][1] != "." && string(Dname[i][1]) != "." && length(Dname[i]) > 5
            if Dname[i][end-4:end] == "mzXML" || Dname[i][end-4:end]== "mzxml" || Dname[i][end-4:end] == "MZXML"
                ind[i]=1
            elseif Dname[i][end-3:end] == "CDF" || Dname[i][end-3:end]== "cdf"
                ind[i]=1

            end
        end

    end
    ind_s=findall(x -> x > 0,ind)
    mz_vales_mat=Array{Any}(undef,size(ind_s,1))
    mz_int_mat=Array{Any}(undef,size(ind_s,1))
    t0_mat=Array{Float64}(undef,size(ind_s,1))
    t_end_mat=Array{Float64}(undef,size(ind_s,1))
    m_mat=Array{Any}(undef,size(ind_s,1))
    Rt_mat=Array{Any}(undef,size(ind_s,1))
    pathin_mat=Array{String}(undef,size(ind_s,1))
    cent_mat = Array{Any}(undef,size(ind_s,1))


    for i=1:size(ind_s,1)


        filenames=[Dname[ind_s[i]]]
        mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,
        Rt,centroid=import_files_MS1(pathin,filenames,mz_thresh,Int_thresh)
        
        mz_vales_mat[i]=mz_vals
        mz_int_mat[i]=mz_int
        t0_mat[i]=t0
        t_end_mat[i]=t_end
        m_mat[i]=m[end-1]
        pathin_mat[i]=pathin
        Rt_mat[i]=Rt
        cent_mat[i]= centroid[1]


    end


    return (mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat,Rt_mat,cent_mat)

end

########################################################################
# Testing area
#

"""

pathin = "/Users/saersamanipour/Desktop/dev/pkgs/ms_import.jl/test"
filenames=["test_file.mzXML"]
mz_thresh=[100,150]
Int_thresh = 100

mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,
Rt = import_files_MS1(pathin,filenames,mz_thresh,Int_thresh)

chrom=import_files(pathin,filenames,mz_thresh,int_thresh)

chrom["MS1"]["Mz_values"]


""" 