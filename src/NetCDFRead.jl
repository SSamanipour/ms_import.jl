using NCDatasets



###############################################################################
## CDF import

function readCDF(File)
    DD=Dict()
    # File="/Users/saersamanipour/Desktop/dev/pkgs/ms_import.jl/test/test_file.CDF"
    ds = Dataset(File)

    scan_acquisition_time=ds["scan_acquisition_time"]
    scan_duration=ds["scan_duration"]                                           #The duration of each scan
    inter_scan_time=ds["inter_scan_time"]                                       #The time between scans
    total_intensity=ds["total_intensity"]                                       #TIC
    mass_range_min=ds["mass_range_min"]                                         #min mass range
    mass_range_max=ds["mass_range_max"]                                         #max mass range
    scan_index=ds["scan_index"]
    point_count=ds["point_count"]
    mass_values = ds["mass_values"]                                             #m/z values
    intensity_values=ds["intensity_values"]                                     #intensity at each m/z value

    #defining the starting point and the end point of the chromatogram
    t0=scan_acquisition_time[1]/60                                              #min
    t_end=scan_acquisition_time[end]/60
    r_t= scan_acquisition_time ./ 60                                      #min

    Max_Mz_displacement=maximum(point_count)
    Mz_values=zeros(length(scan_index),Max_Mz_displacement)
    Mz_intensity=zeros(length(scan_index),Max_Mz_displacement)

    for i=1:length(scan_index)-1
        if (scan_index[i+1] < size(mass_values,1))
            Mz_values[i,1:point_count[i]]=mass_values[scan_index[i]+1:scan_index[i+1]]
            Mz_intensity[i,1:point_count[i]]=intensity_values[scan_index[i]+1:scan_index[i+1]]
        else
            break
        end
        #println(i)
    end

    DD["total_intensity"]=total_intensity
    DD["t0"]=t0
    DD["t_end"]=t_end
    DD["Rt"]=r_t
    DD["Mz_values"]=Mz_values
    DD["Mz_intensity"]=Mz_intensity
    DD["scan_duration"]=scan_duration

    return DD


end



##############################################################################
## The warper function


function cdf_Import(pathin,filenames)
    chrom=Dict()
    for i=1:length(filenames)

        File=joinpath(pathin,filenames[i])
        chrom["MS$i"]=readCDF(File)

    end

    return chrom

end