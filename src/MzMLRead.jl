using LightXML
using Unitful
using Codecs


###############################################################################
# mzML read


function mzml_read(path_in,mz_thresh::Vector{Float32},Int_thresh::Float32)

    xdoc = parse_file(path_in);
    xroot = root(xdoc);
    if !occursin("mzML", LightXML.name(xroot))  
        error("Not an mzML file")
    end
    # Find the msRun node
    mzML_data = find_element(xroot, "mzML");

    el_ins = find_element(mzML_data, "instrumentConfigurationList")
    items = find_element(el_ins, "instrumentConfiguration")

    el_man = attribute(find_element(items, "cvParam"),"name")
    com_list = find_element(items, "componentList")
    if com_list !== nothing
        el_ion = attribute(find_element(find_element(com_list,"source"), "cvParam"),"name")
        Ins = attribute(find_element(find_element(com_list, "analyzer"), "cvParam"),"name")
    else
        el_ion = nothing
        Ins = nothing
    end

    msRun = find_element(find_element(mzML_data, "run"), "spectrumList");
    
    polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int,col_energy,ion_mob=read_scan_mzml(msRun,mz_thresh,Int_thresh)

    chrom=chrom_fold_mzml(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
        totIonCurrent,scan_type,centroided,mz,mz_int,col_energy,ion_mob)

    MS_Instrument=Dict("msModel" => Ins, "msIonisation" => el_ion, "msManufacturer" => el_man)

    chrom["MS_Instrument"]=MS_Instrument
    free(xdoc)
    GC.gc() 
    return chrom

end 

function chrom_fold_mzml(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int,col_energy, ion_mob)

    chrom=Dict{String, Any}()
    ms_levels::Vector{String}=sort(unique(msLevel))
    
    for i=1:length(ms_levels)
        chrom["MS$i"] = Dict(
            "Rt" => round.(retentionTime[msLevel .== ms_levels[i]]./60, digits=3),
            "Polarity" => polarity[msLevel .== ms_levels[i]],
            "PrecursorIon" => pre_mz[msLevel .== ms_levels[i]],
            "PrecursorIonWin" => pre_mz_win[msLevel .== ms_levels[i]],
            "BasePeak" => basePeakMz[msLevel .== ms_levels[i]],
            "TIC" => totIonCurrent[msLevel .== ms_levels[i]],
            "ScanType" => scan_type[msLevel .== ms_levels[i]],
            "Centroid" => centroided[msLevel .== ms_levels[i]],
            "IonMobility" => ion_mob[msLevel .== ms_levels[i]],
            "Mz_values"=> Array{Float32,2}(undef,1,1),
            "Mz_intensity"=>Array{Float32,2}(undef,1,1)
        )
        mz_s=mz[msLevel .== ms_levels[i]]
        mz_int_s=mz_int[msLevel .== ms_levels[i]]
        if i == 2
            chrom["MS2"]["collisionEnergy"] = col_energy[msLevel .== ms_levels[i]]
        end
        chrom["MS$i"]["Mz_values"]=array2mat_(mz_s)
        chrom["MS$i"]["Mz_intensity"]=array2mat_(mz_int_s)


    end

    return chrom

end 

function read_scan_mzml(msRun,mz_thresh::Vector{Float32},Int_thresh::Float32)

    #c=1
    n = 0
    for line in child_elements(msRun)
        n+=1
    end
    polarity2=Vector{Char}(undef, n)
    pre_mz2=Vector{Float32}(undef, n)
    pre_mz_win2=Vector{Float32}(undef, n)
    retentionTime2=Vector{Float32}(undef,n)
    msLevel2=Vector{String}(undef, n)
    basePeakMz2=Vector{Float32}(undef, n)
    totIonCurrent2=Vector{Float32}(undef, n)
    scan_type2=Vector{String}(undef, n)
    centroided2=Vector{String}(undef, n)
    mz2=Vector{Vector{Float32}}(undef, n)
    mz_int2=Vector{Vector{Float32}}(undef, n)
    col_energy2=Vector{Float32}(undef, n)
    ion_mob=Vector{Float32}(undef,n)
    n = 1
    line1 = []
    for line in child_elements(msRun)
            line1 = line
            polarity1,pre_mz1,pre_mz_win1,retentionTime1,msLevel1,basePeakMz1,
            totIonCurrent1,scan_type1,centroided1,mz1,I,colE,ion_mobility=read_scan_info_mzml(line,mz_thresh,Int_thresh)

            polarity2[n]=polarity1
            pre_mz2[n]=pre_mz1
            pre_mz_win2[n]=pre_mz_win1
            retentionTime2[n]=retentionTime1
            msLevel2[n]=msLevel1
            basePeakMz2[n]=basePeakMz1
            totIonCurrent2[n]=totIonCurrent1
            scan_type2[n]=scan_type1
            centroided2[n]=centroided1
            mz2[n]=mz1
            mz_int2[n]= I
            col_energy2[n]=colE
            ion_mob[n] = ion_mobility
            n += 1

    end
    return(polarity2,pre_mz2,pre_mz_win2,retentionTime2,msLevel2,basePeakMz2,
    totIonCurrent2,scan_type2,centroided2,mz2,mz_int2, col_energy2, ion_mob)

end # function

function read_scan_info_mzml(line,mz_thresh::Vector{Float32},Int_thresh::Float32)
    retentionTime = Float32(0.0)
    ion_molbility = Float32(0.0)

    elements::Vector{XMLElement} = collect(child_elements(line))
    # line = find_element(msRun,"scan")
    name_elements1::Vector{Union{String, Nothing}} = attribute.(elements, "name")
    value_elements1::Vector{Union{String, Nothing}} = attribute.(elements, "value")
    pos = name_elements1 .!== nothing
    name_elements::Vector{String} = name_elements1[pos]
    value_elements::Vector{String} = value_elements1[pos]

    if "positive scan" in name_elements
        polarity = '+'
    else
        polarity = '-'
    end 

    msLevel::String = value_elements[name_elements .== "ms level"][1]

    basePeakMz = Float32(-100.00)
    if "base peak m/z" in name_elements
        basePeakMz = parse(Float32, value_elements[name_elements .== "base peak m/z"][1])
    end

    totIonCurrent = Float32(-100.00)
    if "total ion current" in name_elements
       totIonCurrent = parse(Float32, value_elements[name_elements .== "total ion current"][1])
    end

    centroided::String = "0"
    if "centroid spectrum" in name_elements
       centroided= value_elements[name_elements .== "centroid spectrum"][1]
    end

    pre_mz=Float32(0.0)
    pre_mz_win=Float32(0.0)

    mm = find_element(line, "precursorList")
    pre_mz=Float32(0.0)
    pre_mz_win::Float32=Float32(0.0)
    collisionEnergy = Float32(0.0)
    if mm !== nothing
        mm = find_element(mm, "precursor")
        cvParam = find_element(find_element(find_element(mm, "selectedIonList"), "selectedIon"), "cvParam")
        pre_mz = parse(Float32, attribute(cvParam, "value"))
        isolationWindow = find_element(mm, "isolationWindow")
        if isolationWindow !== nothing
            window::Vector{String} = attribute.(collect(child_elements(isolationWindow)), "value")
            pre_mz_win = sum(parse.(Float32, window))
        end
        activation = find_element(mm, "activation")
        activation_data = collect(child_elements(activation))
        pos = attribute.(activation_data, "name") .== "collision energy"
        if any(pos)
            collisionEnergy = parse(Float32, attribute.(activation_data, "value")[pos][1])
        end
    end

    mm = find_element(find_element(line, "scanList"), "scan")
    if mm !== nothing
        for element in child_elements(mm)
            if attribute(element, "accession") == "MS:1000016" # scan start time
                retentionTime = parse(Float32, attribute(element, "value"))
            elseif attribute(element, "accession") == "MS:1002476" # ion mobility drift time
                ion_molbility = parse(Float32, attribute(element, "value"))
            end
        end
    end
   
    element = collect(child_elements(find_element(line, "binaryDataArrayList")))[1]
    element = collect(child_elements(element))
    names = attribute.(element, "name")

    if any(names .== "32-bit float")
        T = Float32
    elseif any(names .== "64-bit float")
        T = Float64
    end
    if any(names .== "zlib compression")
        decode_method = "zlib"
    end

    I = Vector{T}(undef, 0)
    mz = Vector{T}(undef, 0)
    for element in child_elements(find_element(line, "binaryDataArrayList"))
        attr_names = attribute.(collect(child_elements(element)), "name")
        if any(attr_names .== "m/z array")
            scan_data = find_element(element, "binary")
            data = decode(Base64, content(scan_data))
           
            # TODO add here the compression variants like below
            # that is however for mzXML
            #if attribute(peak, "compressionType") ==  "zlib"
            #    data = decode(Zlib, data) 
            #end
            mz::Vector{T} = reinterpret(T, data)
        elseif any(attr_names .== "intensity array")
            scan_data = find_element(element, "binary")
            data = decode(Base64, content(scan_data))
            # TODO add here the compression variants like below
            # that is however for mzXML
            #if attribute(peak, "compressionType") ==  "zlib"
            #    data = decode(Zlib, data) 
            #end
            I::Vector{T} = reinterpret(T, data)
        end
    end
    mz2::Vector{T}=mz[(Int_thresh .<= I) .& (mz_thresh[1] .<= mz .<= mz_thresh[2])]
    I2::Vector{T}=I[(Int_thresh .<= I) .& (mz_thresh[1] .<= mz .<= mz_thresh[2])]
    
    # println(line)
    # TODO
    # scan_type::String = attribute(line, "scanType")
    scan_type = "Full"


    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,totIonCurrent,scan_type,centroided,Float32.(mz2),Float32.(I2),collisionEnergy,ion_molbility)
end